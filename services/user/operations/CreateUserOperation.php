<?php

namespace app\services\user\operations;

use app\models\User;
use app\services\user\repositories\UserRepository;
use app\services\user\services\UserService;

/**
 * Class CreateUserOperation
 * @package app\services\user\operations
 */
class CreateUserOperation
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * sendEmployerCodeOperation constructor.
     * @param UserService $userService
     * @param UserRepository $userRepository
     */
    public function __construct(UserService $userService, UserRepository $userRepository)
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }


    /**
     * @param string $username
     *
     * @param string $phone_number
     *
     * @return bool
     *
     * @throws \Throwable
     */
    public function execute(string $username, string $phone_number): ?User
    {
        $userModel = $this->userService->findOneByUsername($username);

        if ($userModel === null) {
            $this->userRepository->transaction(function () use ($username, $phone_number, &$userModel) {
                $userModel = $this->userService->createUser($username, $phone_number);
            });
        }
        return $userModel;
    }
}