<?php

namespace app\services\user\operations;

use app\services\user\repositories\LinkRepository;
use app\services\user\services\LinkService;

/**
 * Class CreateLinkOperation
 * @package app\services\user\operations
 */
class CreateLinkOperation
{
    /**
     * @var LinkService
     */
    private $linkService;

    /**
     * @var LinkRepository
     */
    private $linkRepository;

    /**
     * sendEmployerCodeOperation constructor.
     * @param LinkService $linkService
     * @param LinkRepository $linkRepository
     */
    public function __construct(LinkService $linkService, LinkRepository $linkRepository)
    {
        $this->linkService = $linkService;
        $this->linkRepository = $linkRepository;
    }


    /**
     * @param int $user_id
     *
     * @param string $old_link_hash
     *
     * @return bool
     *
     * @throws \Throwable
     */
    public function execute(int $user_id, ?string $old_link_hash = null): ?string
    {
        $link_hash = null;
        if($old_link_hash !== null){
            $this->linkService->deactivateLink($old_link_hash);
        }

        $this->linkRepository->transaction(function () use ($user_id, &$link_hash) {
            $link_hash =  $this->linkService->createRandomLink($user_id)->link_hash;
        });
        return $link_hash;
    }
}