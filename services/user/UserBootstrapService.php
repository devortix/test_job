<?php

namespace app\services\user;

use app\base\AbstractBootstrapService;
use app\models\Link;
use app\models\User;
use app\services\user\repositories\LinkRepository;
use app\services\user\repositories\UserRepository;

/**
 * Class UserBuildService
 *
 * @package frontend\services\user
 */
class UserBootstrapService extends AbstractBootstrapService
{
    /**
     * @return array
     */
    protected function services(): array
    {
        return [
            UserRepository::class => [
                ['class' => UserRepository::class],
                [User::class]
            ],
            LinkRepository::class => [
                ['class' => LinkRepository::class],
                [Link::class]
            ],
        ];
    }
}