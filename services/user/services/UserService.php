<?php

namespace app\services\user\services;

use app\models\User;
use app\services\user\repositories\UserRepository;

/**
 * Class UserService
 *
 * @package app\services\user\services
 */
class UserService
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserService constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     *
     * @param string $phone_number
     *
     * @return User
     *
     * @throws \Throwable
     */
    public function createUser(string $username, string $phone_number): User
    {
        /** @var User $model */
        $model = $this->userRepository->createModel();
        $model->username = $username;
        $model->phone_number = $phone_number;
        $this->userRepository->save($model);
        return $model;
    }

    /**
     * @param string $username
     * @return User|null
     */
    public function findOneByUsername(string $username): ?User
    {
        return $this->userRepository->findOneByUsername($username);
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function findOneByPk(int $id): ?User
    {
        return $this->userRepository->findOneBy(['id' => $id]);
    }

}