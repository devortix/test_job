<?php

namespace app\services\user\services;

use app\models\Link;
use app\services\user\repositories\LinkRepository;

/**
 * Class LinkService
 *
 * @package app\services\user\services
 */
class LinkService
{

    /**
     * @var LinkRepository
     */
    private $linkRepository;

    /**
     * UserService constructor.
     *
     * @param LinkRepository $linkRepository
     */
    public function __construct(LinkRepository $linkRepository)
    {
        $this->linkRepository = $linkRepository;
    }

    /**
     * @param int $user_id
     *
     * @return Link
     *
     * @throws \Throwable
     */
    public function createRandomLink(int $user_id): Link
    {
        /** @var Link $model */
        $model = $this->linkRepository->createModel();
        $model->link_hash = uniqid();
        $model->user_id = $user_id;
        $model->activate();
        $this->linkRepository->save($model);
        return $model;
    }

    /**
     * @param string $link_hash
     *
     * @return void
     *
     * @throws \Throwable
     */
    public function deactivateLink(string $link_hash): void
    {
        if($model = $this->findOneByLinkHash($link_hash)){
            $model->deactivate();
            $this->linkRepository->save($model);
        }
    }

    /**
     * @param string $link_hash
     * @return Link|null
     */
    public function findOneByLinkHash(string $link_hash): ?Link
    {
        return $this->linkRepository->findOneByLinkHash($link_hash);
    }

    public function findOneActiveByLinkHash(string $link_hash): ?Link
    {
        return $this->linkRepository->findOneBy([
            'link_hash' => $link_hash,
            'status' => Link::STATUS_ACTIVE,
        ]);
    }

}