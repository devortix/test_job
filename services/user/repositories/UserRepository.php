<?php

namespace app\services\user\repositories;

use app\base\AbstractRepository;
use app\models\User;

/**
 * Class UserRepository
 *
 * @package app\services\user\repositories
 */
class UserRepository extends AbstractRepository
{
    /**
     * @param string $username
     *
     * @return User|null
     */
    public function findOneByUsername(string $username) : ?User
    {
        return $this->findOneBy([
            'username' => $username,
        ]);
    }
}