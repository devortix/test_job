<?php

namespace app\services\user\repositories;

use app\base\AbstractRepository;
use app\models\Link;
use app\models\User;

/**
 * Class LinkRepository
 *
 * @package app\services\user\repositories
 */
class LinkRepository extends AbstractRepository
{
    /**
     * @param string $link_hash
     *
     * @return User|null
     */
    public function findOneByLinkHash(string $link_hash) : ?Link
    {
        return $this->findOneBy([
            'link_hash' => $link_hash,
        ]);
    }
}