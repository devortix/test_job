<?php

namespace app\actions\user;

use app\base\AbstractAction;
use app\controllers\SiteController;
use app\forms\CreateUserForm;
use app\services\user\operations\CreateLinkOperation;
use app\services\user\operations\CreateUserOperation;
use yii\web\Response;

/**
 * Class UserAction
 * @package app\actions\user
 */
class CreateUserAction extends AbstractAction
{

    public const NAME_ACTION = 'create-user';

    /**
     * @var CreateUserOperation
     */
    private $createUserOperation;

    /**
     * @var CreateLinkOperation
     */
    private $createLinkOperation;

    /**
     * CreateUserAction constructor.
     * @param string $id
     * @param SiteController $controller
     * @param CreateUserOperation $createUserOperation
     * @param CreateLinkOperation $createLinkOperation
     * @param array $config
     */
    public function __construct(
        string $id,
        SiteController $controller,
        CreateUserOperation $createUserOperation,
        CreateLinkOperation $createLinkOperation,
        array $config = []
    )
    {
        parent::__construct($id, $controller, $config);

        $this->createUserOperation = $createUserOperation;
        $this->createLinkOperation = $createLinkOperation;
    }

    /**
     * @return string
     *
     * @throws \Throwable
     */
    public function run()
    {
        $form = new CreateUserForm();
        if ($form->load($this->controller->request->post()) && $form->validate()) {
            $user = $this->createUserOperation->execute(
                $form->username,
                $form->phone_number
            );
            if($user->id) {
                $hash = $this->createLinkOperation->execute($user->id);
                return $this->controller->redirect(['site/link', 'hash' => $hash]);
            }
        }

        return $this->controller->render('index' ,['form' => $form]);
    }
}