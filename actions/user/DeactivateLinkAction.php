<?php

namespace app\actions\user;

use app\base\AbstractAction;
use app\controllers\SiteController;
use app\forms\CreateUserForm;
use app\services\user\operations\CreateLinkOperation;
use app\services\user\operations\CreateUserOperation;
use app\services\user\services\LinkService;
use app\services\user\services\UserService;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserAction
 * @package app\actions\user
 */
class DeactivateLinkAction extends AbstractAction
{

    public const NAME_ACTION = 'deactivate-link';

    /**
     * @var LinkService
     */
    private $linkService;


    /**
     * CreateUserAction constructor.
     * @param string $id
     * @param SiteController $controller
     * @param LinkService $linkService
     * @param CreateLinkOperation $createLinkOperation
     * @param array $config
     */
    public function __construct(
        string $id,
        SiteController $controller,
        LinkService $linkService,
        array $config = []
    )
    {
        parent::__construct($id, $controller, $config);

        $this->linkService = $linkService;
    }

    /**
     *
     * @param string $hash
     *
     * @throws \Throwable
     */
    public function run(string $hash)
    {
        if ($this->linkService->findOneByLinkHash($hash) !== null) {
            $this->linkService->deactivateLink($hash);
            return $this->controller->redirect(['site/create-user']);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}