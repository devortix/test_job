<?php

namespace app\actions\user;

use app\base\AbstractAction;
use app\controllers\SiteController;
use app\forms\CreateUserForm;
use app\services\user\operations\CreateLinkOperation;
use app\services\user\operations\CreateUserOperation;
use app\services\user\services\UserService;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserAction
 * @package app\actions\user
 */
class CreateLinkAction extends AbstractAction
{

    public const NAME_ACTION = 'create-link';

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var CreateLinkOperation
     */
    private $createLinkOperation;

    /**
     * CreateUserAction constructor.
     * @param string $id
     * @param SiteController $controller
     * @param UserService $userService
     * @param CreateLinkOperation $createLinkOperation
     * @param array $config
     */
    public function __construct(
        string $id,
        SiteController $controller,
        UserService $userService,
        CreateLinkOperation $createLinkOperation,
        array $config = []
    )
    {
        parent::__construct($id, $controller, $config);

        $this->userService = $userService;
        $this->createLinkOperation = $createLinkOperation;
    }

    /**
     * @return string
     *
     * @param int $user_id
     *
     * @throws \Throwable
     */
    public function run(int $user_id)
    {
        if($this->userService->findOneByPk($user_id) !== null){
            $hash = $this->createLinkOperation->execute($user_id);
            return $this->controller->redirect(['site/link', 'hash' => $hash]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}