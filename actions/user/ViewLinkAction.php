<?php

namespace app\actions\user;

use app\base\AbstractAction;
use app\controllers\SiteController;
use app\forms\CreateUserForm;
use app\services\user\operations\CreateLinkOperation;
use app\services\user\operations\CreateUserOperation;
use app\services\user\services\LinkService;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ViewLinkAction
 * @package app\actions\user
 */
class ViewLinkAction extends AbstractAction
{

    public const NAME_ACTION = 'link';

    /**
     * @var LinkService
     */
    private $linkService;


    /**
     * CreateUserAction constructor.
     * @param string $id
     * @param SiteController $controller
     * @param $linkService LinkService
     * @param array $config
     */
    public function __construct(
        string $id,
        SiteController $controller,
        LinkService $linkService,
        array $config = []
    )
    {
        parent::__construct($id, $controller, $config);
        $this->linkService = $linkService;
    }

    /**
     * @param string $hash
     *
     * @param int $rand
     *
     * @return string
     *
     * @throws \Throwable
     */
    public function run(string $hash, int $rand = null)
    {
        if ($link = $this->linkService->findOneActiveByLinkHash($hash)) {
            if ($rand === 1) {
                $randNumber = rand(0, 1000);
                 if($randNumber % 2){
                    if($randNumber > 900){
                        $sumResult = $randNumber * 0.7;
                    } elseif($randNumber > 600){
                        $sumResult = $randNumber * 0.5;
                    } elseif($randNumber > 300){
                        $sumResult = $randNumber * 0.3;
                    } else {
                        $sumResult = $randNumber * 0.1;
                    }
                     \Yii::$app->session->setFlash('success', "You WIN. Yur winnings are: $sumResult");

                 } else {
                     \Yii::$app->session->setFlash('danger', "You LOSE.");

                 }
                return $this->controller->redirect(['link', 'hash' => $link->link_hash]);

            }
            return $this->controller->render('link', ['link' => $link]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}