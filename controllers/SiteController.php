<?php

namespace app\controllers;

use app\actions\user\CreateLinkAction;
use app\actions\user\CreateUserAction;
use app\actions\user\DeactivateLinkAction;
use app\actions\user\ViewLinkAction;
use app\base\AbstractWebController;

class SiteController extends AbstractWebController
{

    /**
     * @return array
     */
    public function actions()
    {
        return [
            CreateUserAction::NAME_ACTION => [
                'class' => CreateUserAction::class,
            ],
            ViewLinkAction::NAME_ACTION => [
                'class' => ViewLinkAction::class,
            ],
            CreateLinkAction::NAME_ACTION => [
                'class' => CreateLinkAction::class,
            ],
            DeactivateLinkAction::NAME_ACTION => [
                'class' => DeactivateLinkAction::class,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @return array
     */
    protected function accessRules(): array
    {
        return [
            [
                'allow' => true,
                'actions' => [
                    CreateUserAction::NAME_ACTION,
                    'error',
                ],
                'roles' => ['?'],
            ],
        ];
    }
}
