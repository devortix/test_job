<?php

use app\services\user\UserBootstrapService;

return [
    'log',
    UserBootstrapService::class
];