<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Create User';
?>
<div class="site-index">
    <?php $formWidget = ActiveForm::begin(); ?>

    <?= $formWidget->field($form, 'phone_number') ?>
    <?= $formWidget->field($form, 'username') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
