<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Link ' . $link->link_hash;
$this->params['breadcrumbs'][] = $this->title;
$link_str = \yii\helpers\Url::to(['site/link', 'hash' =>  $link->link_hash], true);
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    <?=Html::tag('span','Copy link',   ['class' => 'btn btn-default', 'id' => 'copy_btn']) ?>
    <?=Html::a('Generate link', ['site/create-link', 'user_id' => $link->user_id], ['class' => 'btn btn-success']) ?>
    <?=Html::a('Deactivate link', ['site/deactivate-link', 'hash' => $link->link_hash], ['class' => 'btn btn-danger']) ?>
    <hr>
    <?=Html::a('Play game', ['site/link', 'hash' => $link->link_hash, 'rand' => 1], ['class' => 'btn btn-warning']) ?>

</div>

<?php
$this->registerJs("
    const span = document.getElementById('copy_btn');
span.onclick =  function() {
var inp =document.createElement('input');
document.body.appendChild(inp);
inp.value = '{$link_str}';
inp.select();
document.execCommand('copy',false);
inp.remove();
}
");