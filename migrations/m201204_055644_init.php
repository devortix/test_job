<?php

use yii\db\Migration;

/**
 * Class m201204_055644_init
 */
class m201204_055644_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'phone_number' => $this->string(32)->notNull(),

        ], $tableOptions);

        $this->createTable('{{%link}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'link_hash' => $this->string(32)->notNull()->unique(),
            'status' => $this->tinyInteger()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->addForeignKey(
            'link-user_id-fk',
            '{{%link}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        $this->createIndex('link-link_hash-idx', '{{%link}}', ['link_hash']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%link}}');
        $this->dropTable('{{%user}}');
    }
}
