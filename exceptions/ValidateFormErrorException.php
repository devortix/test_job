<?php

namespace app\exceptions;

use yii\base\Model;

/**
 * Class ValidateFormErrorException
 *
 * @package common\exceptions
 */
class ValidateFormErrorException extends \RuntimeException
{
    /**
     * @var \yii\db\ActiveRecord
     */
    private $model;

    /**
     * SaveModelException constructor.
     *
     * @param \yii\base\Model $model
     * @param string          $message
     * @param int             $code
     * @param \Throwable|null $previous
     */
    public function __construct(Model $model, string $message = null, int $code = 422, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->model = $model;
    }

    /**
     * @return \yii\base\Model
     */
    public function getModel() : Model
    {
        return $this->model;
    }
}