<?php

namespace app\exceptions;

/**
 * Class ServerErrorHttpException
 *
 * @package common\exceptions
 */
class ServerErrorHttpException extends AbstractException
{
    /**
     * @var int
     */
    public $statusCode = 500;

    /**
     * ServerErrorHttpException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message = 'Internal server error.', int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
