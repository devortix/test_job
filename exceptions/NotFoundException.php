<?php

namespace app\exceptions;

/**
 * Class NotFoundException
 *
 * @package common\exceptions
 */
class NotFoundException extends AbstractException
{
    /**
     * @var int
     */
    public $statusCode = 404;

    /**
     * NotFoundException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message = 'Resource not found.', int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
