<?php

namespace app\exceptions;

use yii\web\HttpException;

/**
 * Class AbstractException
 *
 * @package common\exceptions
 */
abstract class AbstractException extends HttpException
{
    public $statusCode;

    /**
     * AbstractException constructor.
     *
     * @param null            $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(
        $message = null,
        $code = 0,
        \Exception $previous = null
    ) {
        parent::__construct($this->statusCode, $message, $code, $previous);
    }
}
