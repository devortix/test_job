<?php

namespace app\exceptions;

/**
 * Class ForbiddenException
 *
 * @package common\exceptions
 */
class ForbiddenException extends AbstractException
{
    /**
     * @var int
     */
    public $statusCode = 403;

    /**
     * ForbiddenException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message = 'Resource forbidden.', int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
