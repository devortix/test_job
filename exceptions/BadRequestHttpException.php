<?php

namespace app\exceptions;

/**
 * Class BadRequestHttpException
 *
 * @package common\exceptions
 */
class BadRequestHttpException extends AbstractException
{
    /**
     * @var int
     */
    public $statusCode = 400;

    /**
     * BadRequestHttpException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message = 'Request is not valid.', int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
