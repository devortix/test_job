<?php

namespace app\exceptions;

use yii\db\ActiveRecord;

/**
 * Class SaveModelException
 *
 * @package app\exceptions
 */
class SaveModelException extends \RuntimeException
{
    /**
     * @var \yii\db\ActiveRecord
     */
    private $model;

    /**
     * SaveModelException constructor.
     *
     * @param \yii\db\ActiveRecord $model
     * @param string               $message
     * @param int                  $code
     * @param \Throwable|null      $previous
     */
    public function __construct(ActiveRecord $model, string $message = null, int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->model = $model;
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getModel() : ActiveRecord
    {
        return $this->model;
    }
}
