<?php

namespace app\exceptions;

/**
 * Class GoneHttpException
 *
 * @package common\exceptions
 */
class GoneHttpException extends AbstractException
{
    /**
     * @var int
     */
    public $statusCode = 410;

    /**
     * GoneHttpException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message = 'Gone.', int $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
