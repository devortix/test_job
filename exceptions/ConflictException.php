<?php

namespace app\exceptions;

/**
 * Class ConflictException
 *
 * @package common\exceptions
 */
class ConflictException extends AbstractException
{
    /**
     * @var int
     */
    public $statusCode = 409;

    /**
     * ConflictException constructor.
     *
     * @param string          $message
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(
        string $message = 'Conflict.',
        int $code = 0,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
