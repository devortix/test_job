<?php

namespace app\forms;
use app\models\User;
use yii\base\Model;

/**
 * UserCreateFormForm
 */
class CreateUserForm extends Model
{

    public $username;
    public $phone_number;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone_number', 'username'], 'required'],
            [['phone_number', 'username'], 'string'],
            ['username', 'unique', 'targetClass' => User::class]
        ];
    }
}
