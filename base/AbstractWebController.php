<?php

namespace app\base;

use yii\base\Module;
use yii\web\Request;

/**
 * Class AbstractWebController
 *
 * @property \yii\web\Request $request
 *
 * @package app\base\controllers
 */
abstract class AbstractWebController extends \yii\web\Controller
{
    /**
     * AbstractWebController constructor.
     *
     * @param mixed                                         $id
     * @param \yii\base\Module                              $module
     * @param array                                         $config
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct(
        string $id,
        Module $module,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);
    }

    /**
     * @param $action
     *
     * @return bool
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action) : bool
    {
        return parent::beforeAction($action);
    }

}
