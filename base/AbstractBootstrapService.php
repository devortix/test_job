<?php

namespace app\base;

/**
 * Class AbstractBootstrapService
 *
 * @package common\contracts
 */
abstract class AbstractBootstrapService implements \yii\base\BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     *
     * @return void
     */
    public function bootstrap($app) : void
    {
        $container = \Yii::$container;
        $container->setSingletons($this->services());
    }

    /**
     * @return array
     */
    protected function services() : array
    {
        return [];
    }
}
