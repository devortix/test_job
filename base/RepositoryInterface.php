<?php

namespace app\base;

use yii\db\ActiveRecord;

/**
 * Interface RepositoryInterface
 *
 * @package common\base
 */
interface RepositoryInterface
{
    /**
     * @return \yii\db\ActiveRecord
     */
    public function createModel() : ActiveRecord;
}
