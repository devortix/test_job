<?php

namespace app\base;

use app\exceptions\SaveModelException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\di\Instance;

/**
 * Class AbstractRepository
 *
 * @package app\base\repository
 */
abstract class AbstractRepository implements RepositoryInterface
{
    /**
     * @var \yii\db\Connection
     */
    protected $connection;

    /**
     * @var string|ActiveRecord
     */
    protected $class;

    /**
     * @var \yii\db\ActiveRecord
     */
    protected $model;

    /**
     * @var \yii\db\ActiveQuery
     */
    protected $activeQuery;

    /**
     * AbstractRepository constructor.
     *
     * @param string $class
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function __construct(string $class)
    {
        $this->connection = Instance::ensure('db', Connection::class);
        $this->class      = $class;
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function createModel() : ActiveRecord
    {
        return new $this->class();
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null) : array
    {
        $this->activeQuery = $this->createQuery();
        $this->activeQuery->where($criteria);
        $this->activeQuery->orderBy($orderBy);

        return $this->activeQuery->limit($limit)->offset($offset)->all($this->connection);
    }

    /**
     * @param array       $criteria
     * @param string|null $orderBy
     * @param int|null    $limit
     * @param int|null    $offset
     *
     * @return array
     */
    public function findByOrderByString(
        array $criteria,
        string $orderBy = null,
        int $limit = null,
        int $offset = null
    ) : array {
        $this->activeQuery = $this->createQuery();
        $this->activeQuery->where($criteria);
        $this->activeQuery->orderBy($orderBy);

        return $this->activeQuery->limit($limit)->offset($offset)->all($this->connection);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return mixed|null
     */
    public function findOneBy(array $criteria, array $orderBy = null) : ?ActiveRecord
    {
        $this->activeQuery = $this->createQuery();
        $this->activeQuery->where($criteria);
        $this->activeQuery->orderBy($orderBy);

        return $this->activeQuery->limit(1)->one($this->connection);
    }

    /**
     * @param array $criteria
     *
     * @return bool
     */
    public function exists(array $criteria) : bool
    {
        $this->activeQuery = $this->createQuery();
        $this->activeQuery->where($criteria);

        return $this->activeQuery->exists($this->connection);
    }

    /**
     * @param array|null $criteria
     *
     * @return int
     */
    public function count(array $criteria = null) : int
    {
        $this->activeQuery = $this->createQuery();
        $this->activeQuery->where($criteria);

        return (int)$this->activeQuery->count('*', $this->connection);
    }

    /**
     * @param string $criteria
     *
     * @return int
     */
    public function countByString(string $criteria) : int
    {
        $this->activeQuery = $this->createQuery();
        $this->activeQuery->where($criteria);

        return (int)$this->activeQuery->count('*', $this->connection);
    }

    /**
     * @param \yii\db\ActiveRecord $model
     * @param bool                 $runValidation
     * @param null                 $attributeNames
     *
     * @return void
     *
     * @throws \Throwable
     */
    public function save(ActiveRecord $model, $runValidation = true, $attributeNames = null) : void
    {
        $this->connection->transaction(static function () use ($model, $runValidation, $attributeNames) {
            if (false === $model->save($runValidation, $attributeNames)) {
                throw new SaveModelException($model);
            }
        });
    }

    /**
     * @param callable    $callable
     * @param string|null $isolationLevel
     *
     * @return bool|null
     * @throws \Throwable
     */
    public function transaction(callable $callable, string $isolationLevel = null) : ?bool
    {
        return $this->connection->transaction($callable, $isolationLevel);
    }

    /**
     * @param string|null $isolationLevel
     *
     * @return \yii\db\Transaction
     */
    public function beginTransaction(string $isolationLevel = null) : \yii\db\Transaction
    {
        return $this->connection->beginTransaction($isolationLevel);
    }

    /**
     * @return \yii\db\Connection
     */
    public function getConnection() : Connection
    {
        return $this->connection;
    }

    /**
     * @return string
     */
    public function getTableName() : string
    {
        return \call_user_func(sprintf('%s::tableName', $this->class));
    }

    /**
     * @param \yii\db\ActiveRecord $model
     * @param array                $counters
     *
     * @return void
     */
    public function updateCounters(ActiveRecord $model, array $counters) : void
    {
        $model->updateCounters($counters);
    }

    /**
     * @param array|null $condition
     * @param array|null $orderBy
     * @param int        $batchSize
     *
     * @return \Generator|ActiveRecord[]
     */
    public function batch(array $condition = null, array $orderBy = null, $batchSize = 500) : ?\Generator
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = $this->createQuery();

        $query->where($condition);
        $query->orderBy($orderBy);

        yield from $query->batch($batchSize, $this->connection);
    }

    /**
     * @param array|null $condition
     * @param array|null $orderBy
     * @param int        $eachSize
     *
     * @return \Generator|ActiveRecord[]
     */
    public function each(array $condition = null, array $orderBy = null, $eachSize = 500) : ?\Generator
    {
        /** @var \yii\db\ActiveQuery $query */
        $query = $this->createQuery();

        $query->where($condition);
        $query->orderBy($orderBy);

        yield from $query->each($eachSize, $this->connection);
    }

    /**
     * @param string $alias
     *
     * @return \yii\db\ActiveQuery
     */
    protected function createQuery(string $alias = 'table') : ActiveQuery
    {
        return call_user_func([$this->class, 'find'])->alias($alias);
    }

    /**
     * @param array $set
     * @param array $condition
     */
    public function updateAll(array $set, array $condition) : void
    {
        $this->class::updateAll($set, $condition);
    }
}
